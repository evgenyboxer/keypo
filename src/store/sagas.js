import { authSagas } from 'core/auth';
import { taskSagas } from 'core/tasks';


export default function* sagas() {
  yield [
    ...authSagas,
    ...taskSagas,
  ];
}
