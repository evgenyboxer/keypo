import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { authReducer } from 'core/auth';
import { tasksReducer } from 'core/tasks';

export default combineReducers({
  auth: authReducer,
  routing: routerReducer,
  tasks: tasksReducer,
});
