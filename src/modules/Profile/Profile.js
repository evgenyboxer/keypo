import React from 'react';

const Profile = ({ children }) => (
  <div>
    {children}
  </div>
);

Profile.propTypes = {
  children: React.PropTypes.element,
};

export default Profile;
