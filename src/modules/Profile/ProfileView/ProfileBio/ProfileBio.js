import React from 'react';

const ProfileBio = () => (
  <div>
    <p>Hi! My name is Tom and lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat!</p>
  </div>
);

export default ProfileBio;
