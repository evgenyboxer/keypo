import React from 'react';

const ProfileBio = () => (
  <div>
    <img src="http://placehold.it/204x204" style={{ borderRadius: '50%' }} alt="" />
    <div><h2>Experienced Airbnb Helper</h2></div>
    <div>
      <span>Tom Jackson</span>
      <span>****</span>
      <span>(12 reviews)</span>
    </div>
    <div>
      <address>Fitzroy, VIC, Australia</address> | <span>Member since December 2016</span>
    </div>
    <ul>
      <li>Personal Check In</li>
      <li>Cleaning & Setup</li>
      <li>Linen Hire</li>
      <li>24/7 Availablity</li>
      <li>Item Storage</li>
    </ul>
  </div>
);

export default ProfileBio;
