import React from 'react';
import ProfileHeader from './ProfileHeader';
import ProfileBio from './ProfileBio';
import ProfileInfo from './ProfileInfo';
import ProfileServices from './ProfileServices';
import ProfileReviews from './ProfileReviews';

const ProfileView = () => (
  <div>
    <ProfileHeader />
    <ProfileBio />
    <ProfileInfo />
    <ProfileServices />
    <ProfileReviews />
  </div>
);

export default ProfileView;
