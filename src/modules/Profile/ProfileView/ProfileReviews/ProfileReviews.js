import React from 'react';

const ProfileReviews = () => (
  <div>
    <h3>12 Reviews</h3>
    <ul>
      <li>Communication</li>
      <li>Value</li>
      <li>Accuracy</li>
    </ul>
    <ul>
      <li>*****</li>
      <li>**</li>
      <li>******</li>
    </ul>
    <hr />
    <div>
      Comments Here...
    </div>
  </div>
);

export default ProfileReviews;
