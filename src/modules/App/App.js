import React from 'react';

import './App.scss';

const App = ({ children }) => (
  <div>
    <main>{children}</main>
  </div>
);

App.propTypes = {
  children: React.PropTypes.element,
};

export default App;
