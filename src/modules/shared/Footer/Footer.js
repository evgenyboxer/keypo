import React from 'react';
import './Footer.scss';

const Footer = () => (
  <section className="footer">
    <div className="container">
      <div className="footer-wrapper">
        <div className="row">
          <div className="col-xs-12 col-sm-6 title-col">
              <h3 className="title">End of the page, start of an era!</h3>
          </div>
          <div className="col-xs-12 col-sm-6">
            <form className="join-form" action="thankyou">
              <div className="row center-xs">
                <input type="text" name="email" placeholder="email@email.com" className="join-form-email"/>
                <button className="join-form-button">Request Early Access</button>
                <div className="error-message">Please enter a valid email</div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Footer;
