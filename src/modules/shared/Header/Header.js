import React from 'react';
import './Header.scss';

const Header = () => (
  <section className="hero">
    <div className="container-fluid">

      <nav className="nav">
        <div className="nav-left">
          <a className="logo" href="/">keypo</a>
        </div>
        <div className="nav-right">
          <a className="become-a-keyper" href="/">Become a Keyper</a>
        </div>
      </nav>

      <div className="headline">
        <div className="row center-xs">
          <h1 className="hero-headline">Find Your Perfect Airbnb Keyper</h1>
        </div>
        <div className="row center-xs">
          <h2 className="hero-copy">That perfect someone to take care of your home and airbnb guests while you’re away</h2>
        </div>
      </div>
    </div>
  </section>
);

export default Header;
