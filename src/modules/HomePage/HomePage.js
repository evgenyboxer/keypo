import React from 'react';
import Header from '../shared/Header';
import Footer from '../shared/Footer';

import './normalize.css';
import './flexboxgrid.css';
import './HomePage.scss';

const HomePage = () => (
  <div>
    <Header />
    <section className="how-it-works">
      <div className="container">
        <h3 className="title">How it works</h3>
        <div className="steps">
          <div className="row">
            <div className="col-xs-12 col-sm-4">
              <div className="step postcode">
                <img src={require('../shared/images/calendar-50@3x.png')} className="icon" alt="" />
                <p className="desc">Enter your dates and postcode</p>
                <img src={require('../shared/images/arrow-down.svg')} className="arrow" alt="" />
              </div>
            </div>
            <div className="col-xs-12 col-sm-4">
              <div className="step select">
                <img src={require('../shared/images/reviewer-male-100@3x.png')} className="icon" alt="" />
                <p className="desc">Select from our reviewed Keypers</p>
                <img src={require('../shared/images/arrow-down.svg')} className="arrow" alt="" />
              </div>
            </div>
              <div className="col-xs-12 col-sm-3">
                <div className="step last-step">
                  <img src={require('../shared/images/airplane-window-open-100@3x.png')} className="icon" alt="" />
                  <p className="desc">Leave your home stress free</p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="testimonials">
      <div className="container">
        <div className="items">
          <div className="row">
            <div className="col-xs-3 col-sm-1 quote-img-col">
              <img src={require('../shared/images/img-quote@3x.png')} className="quote-img" alt="" />
            </div>
            <div className="col-xs-9 col-sm-11 item-col">
              <div className="item">
                <blockquote className="quote">What a brilliant experience. Airbnb paid for our entire 2 month vacation in Tokyo, while our Fitzroy flat and guests were in the safe hands of our Keyper.</blockquote>
                <div className="profile">
                  <div className="profile-pic"><img src={require('../shared/images/lady.png')} alt="" /></div>
                  <div className="profile-info">
                    <div className="profile-info-name">Brianna Terry</div>
                    <div className="profile-info-location">Fitzroy, VIC</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="services">
      <div className="container">

        <h3 className="title">Select a Keyper to take care of your hosting needs</h3>

        <div className="service-list">
          <div className="row">
          <div className="col-xs-12 col-sm-offset-1 col-sm-5">
            <div className="service">
              <div className="row two-lines-in-mobile">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/key.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Personal Check-In and Key Drop-Off</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/arrows.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Between Guest Changeover</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/washing.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Cleaning Services</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/towel.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Linen Hire</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/toilet-paper.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Restock Essentials</span>
                  </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-offset-1 col-sm-5">
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/package.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Personal Item Storage</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/chat.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">24/7 Guest Availability</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/drill.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Property Maintenance</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/edit.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Airbnb Listing Management</span>
                  </div>
              </div>
            </div>
            <div className="service">
              <div className="row">
                  <div className="col-xs-2">
                    <span className="service-icon"><img src={require('../shared/images/house.svg')} alt="" /></span>
                  </div>
                  <div className="col-xs-10">
                    <span className="service-desc">Initial Property Setup</span>
                  </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>

    <section className="benefits">
      <div className="container">

        <h3 className="title">Benefits</h3>

        <div className="items">
          <div className="row center-xs">
            <div className="col-xs-12 col-sm-6">
              <div className="item">
                <div className="item-title">Generate revenue while you are away</div>
                <div className="item-desc">Easily rent out your home when you're out and about and let airbnb help pay for that next trip to Bali.</div>
              </div>
              <div className="item">
                <div className="item-title">Stay in control</div>
                <div className="item-desc">Your home, your airbnb listing - allowing you to monitor reviews, messages and reservations. Transparency is keypo.</div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-6">
              <div className="item">
                <div className="item-title">Find the right Keyper for you</div>
                <div className="item-desc">Keypers are reviewed and experienced, there to get you hosting in no time.</div>
              </div>
              <div className="item">
                <div className="item-title">Carefree short term renting</div>
                <div className="item-desc">From preparing your home for guests to writing a witty review - our Keypers have you covered for whatever you're looking for.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <Footer />
  </div>
);

export default HomePage;

