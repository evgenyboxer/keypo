import App from 'modules/App';
import HomePage from 'modules/HomePage';

import Profile from 'modules/Profile';
import ProfileView from 'modules/Profile/ProfileView';

import paths from './routes.paths';

const getRoutes = () => {
  return {
    path: paths.ROOT,
    component: App,
    childRoutes: [
      {
        indexRoute: {
          component: HomePage,
        },
      },
      {
        path: '/profile',
        component: Profile,
        indexRoute: {
          component: Profile,
        },
        childRoutes: [
          {
            path: '/profile/:userId',
            component: ProfileView,
          },
        ],
      },
    ],
  };
};

export default getRoutes;
