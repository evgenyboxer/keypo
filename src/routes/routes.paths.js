export default {
  ROOT: '/',
  SIGN_IN: '/sign-in',
  PROFILE_PAGE: '/profile',
};
